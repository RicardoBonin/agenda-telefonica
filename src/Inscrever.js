import React, { useState } from "react";
import Wrapper from "./Wrapper";

const valoresIniciais = {
  nome: "",
  email: "",
  password: ""
};

const Inscrever = props => {
  const [valores, setValores] = useState(valoresIniciais);

  const handleChange = e => {
    const { name, value } = e.target;
    setValores({ ...valores, [name]: value });
  };

  const cadastrar = e => {
    e.preventDefault();
    console.log("Formulario submetido e salvo no localstorage");
    localStorage.setItem("nome", JSON.stringify(valores.nome));
    localStorage.setItem("email", JSON.stringify(valores.email));
    localStorage.setItem("password", JSON.stringify(valores.password));
    setValores(valoresIniciais);
    props.history.push("/contatos");
  };

  return (
    <Wrapper>
      <div className="container">
        <h2>Inscreva-se</h2>
        <form onSubmit={cadastrar}>
          <label>Nome: </label>
          <input
            name="nome"
            type="text"
            className="nome"
            value={valores.nome}
            onChange={handleChange}
          ></input><br></br>
          <label>E-mail: </label>
          <input
            name="email"
            type="email"
            className="email"
            value={valores.email}
            onChange={handleChange}
          ></input><br></br>
          <label>Senha: </label>
          <input
            name="password"
            type="password"
            className="senha"
            value={valores.password}
            onChange={handleChange}
          ></input>
          <br></br>
          <input
            type="submit"
            className="btn-submit"
            value="Criar Conta"
          ></input>
        </form>
      </div>

      <style jsx="true">
        {`
          body {
          }
          .container{
            padding: 40px;
            border: 2px solid black;
            border-radius: 10px;
            width: 30%;
            margin: auto;
            margin-top: 40px;
          }
          div {
            background-color: #232324;
            color: white;
            text-align: center;
          }

          input{
            width: 50%;
            padding: 5px;
            border-radius: 5px;
            margin-top: 4px;
          }
          .btn-submit {
            margin-left: 50px;
            width: 40%;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default Inscrever;
