import React from "react";
import Wrapper from "./Wrapper";
import {contatos} from './contatos.json'

const TabelaContatos = () => {
  if (contatos.length !== 0) {
    return(
      <div>
        { 
          contatos.map((contato, i) => {
            return(
              <table key={i}className='tabela'>
                <thead>
                  <tr>
                    <th><h2 className='cont'>{contato.nome}</h2></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><p className='num'>{contato.celular}</p></td>
                    <td><p className='cidade'>{contato.cidade}-{contato.estado}</p></td>
                  </tr>
                </tbody>
              </table>
            )
          })
        }
      </div>
    )
  }else {
    return (
      <span>Você não possui contatos!</span>
    )
  }
} 
const Contatos = () => {
  return (
    <Wrapper>
      <div className='container'>
        <h2>Meus Contatos</h2>
        <TabelaContatos />
      </div>
      <style jsx="true">
        {`.container{
          padding: 40px;
          border: 2px solid black;
          border-radius: 10px;
          margin: auto;
          margin-top: 40px;
        }
        div {
          background-color: #232324;
          color: white;
        }
          h2 {
            text-align: center;
          }
          .cont{
            text-align: left;
          }
          .tabela{
            width: 600px;
          }
          .cidade{
            text-align: right;
          }
          table {
            border-bottom: 1px solid #ddd;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default Contatos;
