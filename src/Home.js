import React from "react";
import { Link } from "react-router-dom";
import Wrapper from "./Wrapper";

const Home = () => {
  return (
    <Wrapper>
      <div className="container">
        <div>
          <h1>Sua agenda telefonica.</h1>
          <p>
            Com esta agenda você poderá armazenar seus contatos com o nome, o
            sobrenome, o telefone e e-mail. Poderá ainda, busca-los facilmente e
            com um click abrirá o whatsapp com o contato.
          </p>
        </div>
        <div>
          <Link to="/login">
            <button className="btn">Log In</button>
          </Link>
          {/* Mais tarde usaremos a linha seguinte *}
          {/* <Link to="/login">Log In</Link> */}

          {/* <button className="btn">Sign In</button> */}
          <Link to="/inscrever">
            <button className="btn">Sign In</button>
          </Link>
        </div>
      </div>
      <style jsx="true">
        {`
          .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
          }
          div {
            background-color: #232324;
            color: white;
            text-align: center;
          }
          h1 {
            font-size: 4em;
          }
          p {
            text-align: center;
            font-size: 2em;
          }
          .btn {
            margin-left: 30px;
            padding: 10px;
            border-radius: 5px;
            font-size: 2em;
            cursor: pointer;
            background-color: #7e7e7e;
            color: #fff;
            border-color: #7e7e7e;
          }
          .btn:hover {
            background-color: #4d4c4c;
            border-color: #4d4c4c;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default Home;
