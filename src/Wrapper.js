import React from "react";

const Wrapper = ({ children }) => {
  return (
    <div className="wrapper">
      {children}
      <style jsx="true">{`
        .wrapper {
          display: flex;
          justify-content: center;
          padding: 0 5em;
          height: calc(100% - 80px);
        }
      `}</style>
    </div>
  );
};

export default Wrapper;
