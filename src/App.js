import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import Header from "./Header";
import Home from "./Home";
import Sobre from "./Sobre";
import Contatos from "./Contatos";
import Inscrever from "./Inscrever";
import Login from "./Login";
const App = () => {
  return (
    <Router>
      <Header title="Agenda Telefonica" />
      <Route exact path="/" component={Home} />
      <Route path="/sobre" component={Sobre} />
      <Route path="/contatos" component={Contatos} />
      <Route path="/inscrever" component={Inscrever} />
      <Route path="/Login" component={Login} />
      <style jsx="true">{`
        html,
        body,
        #root {
          height: 100%;
        }
      `}</style>
    </Router>
  );
};

export default App;
