import React, { useState } from "react";
import Wrapper from "./Wrapper";
import { Redirect } from "react-router-dom";

const MostraLogin = () => {
  return <h2>Login</h2>;
};
const MostraLabel = props => <label> {props.text}</label>;
const Login = () => {
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [success, setSuccesse] = useState(false);

  const emailChange = evt => {
    setEmail(evt.target.value);
  };
  const senhaChange = evt => {
    setSenha(evt.target.value);
  };

  const logar = e => {
    e.preventDefault();
    if (email.length === 0 || senha.length === 0) {
      alert(`[ERRO]: Por favor, preencha todos os campos!`);
    } else {
      const lemail = JSON.parse(localStorage.getItem("email", ""));
      const lsenha = JSON.parse(localStorage.getItem("password", ""));
      console.log(lemail);
      console.log(lsenha);
      console.log("---------");
      console.log(email);
      console.log(senha);
      if (email === lemail && senha === lsenha) {
        alert("login ok!");
        setSuccesse(true);
      } else {
        alert("[ERRO]: Login ou senha errado. Tente novamente!");
      }
    }
  };

  if (success) {
    return <Redirect to="/contatos" />;
  }

  return (
    <Wrapper>
      <div className="container">
        <MostraLogin />
        <form>
          <MostraLabel text="E-mail" />
          <input
            name="email"
            type="email"
            className="email"
            value={email}
            onChange={emailChange}
          ></input>
          <MostraLabel text="Senha" />
          <input
            name="password"
            type="password"
            className="senha"
            value={senha}
            onChange={senhaChange}
          ></input>
          <br />
          <button className="btn" onClick={logar}>
            Fazer Login
          </button>
        </form>
      </div>

      <style jsx="true">
        {`
          body {
          }
          .container {
            padding: 40px;
            border: 2px solid black;
            border-radius: 10px;
            width: 30%;
            margin: auto;
            margin-top: 40px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          div {
            background-color: #232324;
            color: white;
            text-align: center;
          }

          input {
            width: 50%;
            padding: 5px;
            border-radius: 5px;
            margin-top: 4px;
          }
          .btn {
            width: 40%;
            padding: 5px;
            border-radius: 5px;
            margin-top: 4px;
          }
        `}
      </style>
    </Wrapper>
  );
};

export default Login;
