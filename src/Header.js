import React from "react";
import { Link } from "react-router-dom";

const Header = ({ title }) => {
  return (
    <header>
      <h2 className="title">{title}</h2>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/contatos">Contatos</Link>
        </li>
        <li>
          <Link to="/sobre">Sobre</Link>
        </li>
      </ul>
      <style jsx="true">{`
        header {
          display: flex;
          background-color: #000;
          align-items: center;
          padding: 0 5em;
        }
        .title {
          width: 20%;
          color: #fff;
        }
        ul {
          display: flex;
          list-style-type: none;
          margin: 0;
          padding: 0;
          justify-content: flex-end;
          width: 80%;
        }
        li {
          margin-right: 0.3em;
          font-size: 20px;
          padding: 1em 0;
          border-right: 1px solid #aaaaaa;
        }
        li a {
          text-decoration: none;
          color: #fff;
          padding: 1em;
        }
        li a:hover {
          color: wheat;
        }
      `}</style>
    </header>
  );
};

export default Header;
